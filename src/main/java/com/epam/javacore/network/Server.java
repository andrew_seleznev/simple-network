package com.epam.javacore.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Server {

	ServerSocket welcomeSocket;
	Socket connectionSocket;
	BufferedReader inFromClient;
	DataOutputStream outToClient;
	static Logger logger = LogManager.getLogger(Server.class);

	public static void main(String argv[]) throws Exception {
		Server server = new Server();
		server.startServer();

	}

	public void startServer() {
		try {
			welcomeSocket = new ServerSocket(6789);
			connectionSocket = welcomeSocket.accept();
			System.out.println("Accept check");
			inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			Thread t = new Thread(new RemoteServer());
			t.start();
			printDefaultInformation();
		} catch (IOException e) {
			logger.error("Impossible to start server", e);
		}

	}

	private void printDefaultInformation() {
		System.out.println("Server application has been started successfully...");
	}

	private class RemoteServer implements Runnable {

		@Override
		public void run() {
			String clientSentence;
			try {
				while (true) {
					clientSentence = inFromClient.readLine();
					if (clientSentence == null) {
						break;
					}
					System.out.println("Received: " + clientSentence);
					outToClient.writeBytes(clientSentence + '\n');
				}
			} catch (IOException e) {
				logger.error("Couldn't close a socket", e);
			} finally {
				try {
					inFromClient.close();
					outToClient.close();
					connectionSocket.close();
				} catch (IOException e) {
					logger.error("Connection closed", e);
				}
			}
		}
	}

}
