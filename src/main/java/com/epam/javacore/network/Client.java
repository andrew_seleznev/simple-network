package com.epam.javacore.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;

import javax.net.SocketFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Client {

	private Socket clientSocket;
	private DataOutputStream outToServer;
	private BufferedReader inFromServer;

	private static Scanner scanner = new Scanner(System.in, "UTF-8");
	static Logger logger = LogManager.getLogger(Client.class);

	public static void main(String argv[]) throws Exception {
		Client client = new Client();
		client.doConnect();
	}

	public void doConnect() {
		try {
			clientSocket = SocketFactory.getDefault().createSocket("localhost", 6789);
			outToServer = new DataOutputStream(clientSocket.getOutputStream());
			inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			Thread t = new Thread(new RemoteReader());
			t.start();
			printDefaultInformation();
		} catch (Exception e) {
			logger.error("Unable to connect to server!", e);
		}
	}

	private String readStringInput(String prefix) {
		System.out.print(prefix);
		String line = scanner.nextLine();
		return line;
	}

	private int readUserActionInput(int maxInput) {
		do {
			int action = readIntInput("Please, input desired action: ", maxInput);
			if (action >= 0 && action <= maxInput) {
				return action;
			}
		} while (true);
	}

	private int readIntInput(String string, final int max) {
		String s = readStringInput(string);
		int pos = Integer.parseInt(s);
		if (pos > max) {
			System.out.println("Please, choose a valid option!");
		}
		return pos;
	}

	private int printMainActions() {
		System.out.println(" 1) Send message to the server");
		System.out.println(" 2) Exit");
		return 2;
	}

	private void sendMessage() {
		String sentence;
		String modifiedSentence;
		try {
			System.out.print("Type a message: ");
			sentence = scanner.nextLine();
			outToServer.writeBytes(sentence + '\n');
			modifiedSentence = inFromServer.readLine();
			System.out.println("FROM SERVER: " + modifiedSentence);
		} catch (IOException e) {
			logger.error("Error while reading from server.", e);
		}
	}

	private void close() {
		try {
			outToServer.close();
			inFromServer.close();
			clientSocket.close();
		} catch (IOException e) {
			logger.error("An error occurred while socket closing", e);
		}
	}

	private void printDelimiter() {
		System.out.println("--------------------------------------------");
	}

	private void printDefaultInformation() {
		System.out.println("Client application has been started successfully...");
	}

	private class RemoteReader implements Runnable {

		@Override
		public void run() {
			int action = 0;
			do {
				printDelimiter();
				System.out.println("What would you like to do?");
				int maxInput = printMainActions();
				action = readUserActionInput(maxInput);
				if (action == 1) {
					sendMessage();
				} else if (action == 2) {
					close();
					printDelimiter();
					System.out.println("Client work has been completed...");
					break;
				}
			} while (true);
		}
	}
}
